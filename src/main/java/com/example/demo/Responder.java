package com.example.demo;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

public class Responder<T> {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDateTime datee;
    private HttpStatus status;
    private String mes;
    private T payload;

    public Responder() {
    }

    public Responder(LocalDateTime datee, HttpStatus status, String mes, T payload) {

        this.datee = datee;
        this.status = status;
        this.mes = mes;
        this.payload = payload;

    }

    public LocalDateTime getDatee() {
        return datee;
    }

    public void setDatee(LocalDateTime datee) {
        this.datee = datee;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }
}

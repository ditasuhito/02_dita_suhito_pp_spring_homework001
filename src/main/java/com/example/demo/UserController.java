package com.example.demo;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@RestController
public class UserController {

    ArrayList<User> users = new ArrayList<>(){{
        add(new User("To", "M", 12, "PP"));
        add(new User("ok", "M", 12, "PP"));
        add(new User("yes", "M", 12, "PP"));
    }};



    @GetMapping("/users")
    public List<User> getAll() {
        return users;
    }

    @PostMapping("/users/add")
    public ResponseEntity<?> insert(@RequestBody UserReq userReq) {
        Responder responder = new Responder<>();
        responder.setMes("Add Successfully");
        responder.setStatus(HttpStatus.OK);
        responder.setDatee(LocalDateTime.now());
        users.add(new User(userReq.getName(), userReq.getGender(), userReq.getAge(), userReq.getAddress()));
        return ResponseEntity.ok().body(users);
    }

    @GetMapping("/usersid/{id}")
    public ResponseEntity<?> getUserById(@PathVariable("id") Integer ppid) {
        for (User rs : users
        ) {
            if (rs.getId() == ppid) {

                return ResponseEntity.ok(new Responder<User>(LocalDateTime.now(), HttpStatus.OK, "Update Successfully", rs));
            }
        }
        return null;
    }

    @GetMapping("/users/search")
    public ResponseEntity<User> findByname(@RequestParam String name) {
        for (User rs : users
        ) {
            if (rs.getName().equals(name)) {
                return ResponseEntity.ok(new Responder<User>(LocalDateTime.now(), HttpStatus.OK, "Successfully", rs).getPayload());
            }

        }
        return null;
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<Responder<User>> update(@RequestBody UserReq updateUser, @PathVariable("id") Integer ppid) {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getId() == ppid) {
                users.set(ppid-1, new User(ppid,updateUser.getName(), updateUser.getGender(),updateUser.getAge(), updateUser.getAddress()));
                return ResponseEntity.ok(new Responder<User>(LocalDateTime.now(), HttpStatus.OK, "Update Successfully", users.get(ppid-1)));
            }
        }
        return null;
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deletePost(@PathVariable("id") Integer ppid, @RequestBody User delete) {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getId() == ppid) {
                users.remove(i);
                return ResponseEntity.ok(new Responder<ArrayList<User>>(LocalDateTime.now(), HttpStatus.OK, "Deleted Successfully", users));
            }

        }
        return ResponseEntity.ok("Delete id Not found");
    }


}








